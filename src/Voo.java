
import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class Voo implements Serializable {

	private static int count=0;
	private int id, lugares, data_partida;
	private String local_partida, local_destino;
	private ArrayList<String> listapassageiros;

	public Voo(int lugares, String local_partida, String local_destino, String data_partida){
		id=count;
		count++;
		this.lugares=lugares;
		this.local_partida=local_partida.toUpperCase();
		this.local_destino=local_destino.toUpperCase();
		this.data_partida=Integer.parseInt(data_partida);
		listapassageiros=new ArrayList<String>();
	}

	public Voo(int lugares, String local_partida, String local_destino, String data_partida, ArrayList<String> listapassageiros){
		id=count;
		count++;
		this.lugares=lugares;
		this.local_partida=local_partida.toUpperCase();
		this.local_destino=local_destino.toUpperCase();
		this.data_partida=Integer.parseInt(data_partida);
		this.listapassageiros=listapassageiros;
	}

	public void setLugares(int lugares){
		this.lugares=lugares;
	}

	public void setLocalPartida(String local_partida){
		this.local_partida=local_partida;
	}

	public void setLocalDestino(String local_destino){
		this.local_destino=local_destino;
	}

	public void setDataPartida(int data_partida){
		this.data_partida=data_partida;
	}

	public void setListaPassageiros(ArrayList<String> listapassageiros){
		this.listapassageiros=listapassageiros;
	}

	public void addPassageiros(String nomes){
		String[] listanomes=nomes.split(";");
		for(int i=0;i<listanomes.length;i++)
			listapassageiros.add(listanomes[i]);
	}

	public int getId(){
		return id;
	}

	// calcula o número de lugares disponíveis de um dado vôo
	public int getLugares(){
		return this.lugares-listapassageiros.size();
	}

	public String getLocalPartida(){
		return this.local_partida;
	}

	public String getLocalDestino(){
		return this.local_destino;
	}

	public int getDataPartida(){
		return this.data_partida;
	}

	// retorna a lista de passageiros do vôo
	public ArrayList<String> getListaPassageiros(){
		return this.listapassageiros;
	}

	// retorna o número de passageiros do vôo
	public int getNrPassageiros(){
		return this.listapassageiros.size();
	}

	public String toString(){
		String s="";
		s+="(id:"+id+" | destino:"+local_destino+" | data:"+data_partida+" | lugares:"+getLugares()+")";
		return s;
	}

}
