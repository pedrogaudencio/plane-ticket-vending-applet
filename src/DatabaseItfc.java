
import java.rmi.RemoteException;
import java.util.ArrayList;

public interface DatabaseItfc extends java.rmi.Remote {

	public void adicionaVoo(Voo v) throws RemoteException;
	public Voo getVooPorId(int id) throws RemoteException;
	public ArrayList<Voo> getVooPorData(String d) throws RemoteException;
	public ArrayList<Voo> getVooPorDestino(String d) throws RemoteException;
	public String compraLugares(int id, int nr, String nomes) throws RemoteException;
	public ArrayList<String> consultaPassageiros(int id) throws RemoteException;

}
