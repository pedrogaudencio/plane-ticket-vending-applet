
import java.rmi.server.*;

public class Server {

	public static void main(String args[]) {

		int regPort= 1099; // default

		if (args.length !=1) { // obrigar a presenca de um argumento
			System.out.println("Usage: java so2.trabalho1.servidor.Server registryPort");
			System.exit(1);
		}


		try {
			regPort=Integer.parseInt(args[0]);


			// criar um Objecto Remoto
			Database db = new Database();
			Voo v1=new Voo(2,"Beja","Lisboa","20120427");
			Voo v2=new Voo(50,"Beja","Bahamas","20120428");
			Voo v3=new Voo(50,"Beja","baHAmas","20120428");
			Voo v4=new Voo(50,"Beja","BahaMAS","20120428");
			Voo v5=new Voo(20,"Beja","Lisboa","20120727");
			Voo v6=new Voo(18,"Beja","CABO VErde","20120428");
			Voo v7=new Voo(5,"Beja","Polonia","20120628");
			Voo v8=new Voo(17,"Beja","Brasil","20120428");
			Voo v9=new Voo(14,"Beja","Toronto","20120427");
			Voo v10=new Voo(40,"Beja","Las PAlmas","20120428");
			Voo v11=new Voo(50,"Beja","Colombia","20120521");
			Voo v12=new Voo(60,"Beja","Mexico","20120520");
			db.adicionaVoo(v1);
			db.adicionaVoo(v2);
			db.adicionaVoo(v3);
			db.adicionaVoo(v4);
			db.adicionaVoo(v5);
			db.adicionaVoo(v6);
			db.adicionaVoo(v7);
			db.adicionaVoo(v8);
			db.adicionaVoo(v9);
			db.adicionaVoo(v10);
			db.adicionaVoo(v11);
			db.adicionaVoo(v12);

			// exportar o objecto para que ele possa receber pedidos
			DatabaseItfc stub = (DatabaseItfc) UnicastRemoteObject.exportObject(db);
			System.out.println("Created RMI object");




			// registar este objecto no servico de nomes, para que os clientes
			// possam obter facilmente a sua referencia remota

			// usar o Registry local do porto regPort
			java.rmi.registry.Registry registry = java.rmi.registry.LocateRegistry.getRegistry(regPort);

			// bind
			registry.rebind("voos", stub);  // NOME DO SERVICO


			System.out.println("Bound RMI object in registry");
			System.out.println("servidor pronto");

		} 

		catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
