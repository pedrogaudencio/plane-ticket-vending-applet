
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Iterator;

public class JanelaCliente implements ActionListener {

	String DEV_EMAIL="jsaias@uevora.pt";
	String APP_NAME="Novo Aeroporto de Evora";    

	protected static String ENVIAR= "ENVIAR";
	protected static String SAIR= "SAIR";

	@SuppressWarnings("unused")
	private JTextArea qresult,tRef1,tRef2,tRef3,lnomes2,tDescr1,tUnid2,tDescr4;
	private static final Font fontLabel= new Font("Arial",Font.PLAIN,11);
	private JEditorPane outPane= null;


	private JFrame frame;
	JRadioButton rbt1= new JRadioButton("pesquisa por destino");
	JRadioButton rbt2= new JRadioButton("compra N lugares");
	JRadioButton rbt3= new JRadioButton("consultar lista de passageiros");
	JRadioButton rbt4= new JRadioButton("consultar voos numa data");
	ButtonGroup bgroup;

	int LARGURA=665;
	int ALTURA=600;

	DatabaseItfc remotedb;

	public JanelaCliente() {
		bgroup = new ButtonGroup();
		bgroup.add(rbt1);
		bgroup.add(rbt2);
		bgroup.add(rbt3);
		bgroup.add(rbt4);
	}

	public void setRemoteOjb(DatabaseItfc obj){
		remotedb = obj;
	}

	public void initX( ) {
		frame= new JFrame( APP_NAME );
		frame.setSize( new Dimension(LARGURA,ALTURA) );
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	
		frame.setLocationRelativeTo(null);

		tRef1= new JTextArea(1,8);

		tDescr1= new JTextArea(1,20);

		tRef2= new JTextArea(1,8);
		tUnid2= new JTextArea(1,4);
		lnomes2= new JTextArea(1,8);

		tRef3= new JTextArea(1,8);

		tDescr4= new JTextArea(1,12);

		// campo para mostrar informacao, nao editavel
		qresult= new JTextArea(8,70);       
		qresult.setEditable(false);

		// mostrar
		updateFrame();
	}

	// actualizar o frame de acordo com o estado
	private void updateFrame() {        

		try {
			if (outPane != null)
				frame.setSize( new Dimension(LARGURA,ALTURA) );

			JPanel jp = new JPanel( new BorderLayout() );

			Component centro= desenhaCentro();
			jp.add(centro, BorderLayout.CENTER);

			frame.setContentPane(jp);
		}
		catch (Exception e) {
			System.err.println("ERROR while updating frame: "+e);
			e.printStackTrace();
			System.exit(1);
		}
		// actualizar ==========
		if (outPane == null)
			frame.pack();
		frame.setVisible(true);
		// ---------------------
		//System.out.println("          == PAINEL ACTUALIZADO");
	}

	private Component desenhaCentro()
			throws Exception
			{
		if (outPane != null) {    // done, falta so' mostrar
			JScrollPane scrollPane = new JScrollPane(outPane); 
			return scrollPane;
		}
		else {    //
			JPanel buttonP = new JPanel(new BorderLayout());	

			JPanel p = new JPanel(new BorderLayout());
			//p.setBackground(Color.yellow);

			JPanel p1 = new JPanel(new BorderLayout());
			JPanel p12 = new JPanel(new BorderLayout());
			JPanel p3 = new JPanel(new BorderLayout());
			JPanel p4 = new JPanel(new BorderLayout());
			JPanel p2= new JPanel(new GridLayout(1,8));

			// *************************
			// campos das operacoes
			JPanel pbase= new JPanel(new GridLayout(1,5,2,3));
			pbase.setBackground(Color.yellow);

			JPanel pbase1= new JPanel(new GridLayout(3,1));
			pbase1.add( rbt1 );
			pbase1.add( new JLabel("Destino:") );
			pbase1.add( tRef1 );
			pbase.add( pbase1 );

			JPanel pbase2= new JPanel(new GridLayout(7,1));
			pbase2.add( rbt2 );
			pbase2.add( new JLabel("ID do vôo:") );
			pbase2.add( tRef2 );
			pbase2.add( new JLabel("Unidades:") );
			pbase2.add( tUnid2 );
			pbase2.add( new JLabel("Nome(s) - usar ; se tiver mais de um nome:") );
			pbase2.add( lnomes2 );

			pbase.add( pbase2 );

			JPanel pbase3= new JPanel(new GridLayout(5,1));
			pbase3.add( rbt3 );
			pbase3.add( new JLabel("ID do Vôo:") );
			pbase3.add( tRef3 );
			pbase.add( pbase3 );

			JPanel pbase4= new JPanel(new GridLayout(5,2));
			pbase4.add( rbt4, BorderLayout.WEST );
			pbase4.add( new JLabel("Data (AAAAMMDD):"), BorderLayout.WEST );
			//final JTextField text = new JTextField(20);
			JButton escolherdata = new JButton("Escolher data");
			pbase4.add(tDescr4, BorderLayout.WEST);
			pbase4.add(escolherdata, BorderLayout.EAST);
			//pbase4.add(escolherdata);
			final JFrame f = new JFrame();
			//f.getContentPane().add(pbase4);
			//f.pack();
			//f.setVisible(true);

			escolherdata.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					tDescr4.setText(new DatePicker(f).setPickedDate());	
				}
			});

			pbase4.add( escolherdata );
			pbase.add( pbase4 );


			p2.setAlignmentX(JPanel.CENTER_ALIGNMENT);
			// *************************

			p12.add( pbase , BorderLayout.WEST );
			p12.add( p1 , BorderLayout.CENTER );

			// ****************
			JLabel lch= new JLabel("resultado");
			lch.setFont( fontLabel);
			p2.add(qresult);
			p2.setAlignmentX(JPanel.CENTER_ALIGNMENT);

			p3.add(lch,BorderLayout.NORTH );
			p3.add(p2,BorderLayout.CENTER );

			JButton button = new JButton(" enviar ");
			button.addActionListener(this); 
			button.setActionCommand(ENVIAR);

			buttonP.add(button,BorderLayout.CENTER );	

			p4.add(p3,BorderLayout.NORTH );
			p4.add(buttonP,BorderLayout.CENTER );
			// ****************

			p.add( new JLabel("Operações para o Serviço de Peças") , BorderLayout.NORTH );
			p.add( p12 , BorderLayout.WEST );
			p.add( p4 , BorderLayout.SOUTH ); // resultado e botao enviar
			return p;
		}
			}

	// EVENTO SOBRE O FRAME "ACTION"
	public void actionPerformed(ActionEvent e) {
		String command = e.getActionCommand();
		//System.out.println("COMMAND: "+command);
		// ********************************************

		try {
			if ( command.equals(SAIR) ) {
				frame.dispose();
				System.exit(0);
			}


			if ( command.equals(ENVIAR) ) {
				try {
					String s="";

					// que operacao esta seleccionada?
					if (rbt1.isSelected()) {  // pesquisa voos por destino
						s+= "Pesquisa por destino: "+ tRef1.getText()+"\n";
						String[] dest=tRef1.getText().split("\n");
						ArrayList<Voo> voos=remotedb.getVooPorDestino(dest[0]);
						if(voos.isEmpty())
							s+="Destino sem vôos marcados.";
						else{
							Iterator<Voo> itr=voos.iterator();
							while(itr.hasNext())
								s+="\n"+itr.next();
						}
					}

					else if(rbt2.isSelected()) { // compra N lugares
						int id, unid;
						id=Integer.parseInt(tRef2.getText());
						unid=Integer.parseInt(tUnid2.getText());
						System.out.println("cenas");
						s+="Pesquisa por data: \n"+
								remotedb.compraLugares(id, unid, lnomes2.getText());
					}

					else if(rbt3.isSelected()) { // consultar lista passageiros por voo
						int id=Integer.parseInt(tRef3.getText());
						if(remotedb.getVooPorId(id)==null)
							s+="Vôo inexistente ou simulação de 9/11.";
						else{
							ArrayList<String> listapassageiros=remotedb.consultaPassageiros(id);
							s+="Consultar passageiros do vôo"+remotedb.getVooPorId(id).toString()+":\n";
							if(listapassageiros.isEmpty())
								s+="Lista de passageiros vazia.";
							else{
								Iterator<String> itr=listapassageiros.iterator();
								while(itr.hasNext())
									s+="\n"+itr.next();
							}
						}
					}

					else if(rbt4.isSelected()) { // procura voos por data
						s+= "Pesquisa por data: "+ tDescr4.getText()+"\n";
						ArrayList<Voo> voos=remotedb.getVooPorData(tDescr4.getText());
						if(voos.isEmpty())
							s+="Data sem vôos marcados.";
						else{
							Iterator<Voo> itr=voos.iterator();
							while(itr.hasNext())
								s+="\n"+itr.next();
						}
					}

					System.err.println("DEBUG: "+s);
					if ( s != null )
						qresult.setText(s);
				}
				catch (NumberFormatException nfException) {
					qresult.setText("Insira uma data válida.");
					nfException.printStackTrace();
				}
				catch (Exception eNum) {
					qresult.setText("problemas: "+eNum.getMessage());
					eNum.printStackTrace();
				}
				//System.err.println("------------------------------------");
				updateFrame();
			}

			//resetFocusCheck();

		}
		catch (Exception e1) {
			System.err.println(e1);
			try{
				Thread.sleep(1000);
			}
			catch(Exception e2){
			}
			System.exit(0);
		}
	}

}
