
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Hashtable;

public class Database implements DatabaseItfc {

	ArrayList<Integer> listadestinos, listadata;
	Hashtable<Integer, ArrayList<Integer>> destinotable, datatable;
	Hashtable<Integer, Voo> voo;

	public Database(){
		destinotable=new Hashtable<Integer, ArrayList<Integer>>();
		datatable=new Hashtable<Integer, ArrayList<Integer>>();
		voo=new Hashtable<Integer, Voo>();
	}

	// adiciona vôo na Base de Dados
	public void adicionaVoo(Voo v) throws RemoteException {
		System.err.println("invocacao adicionaVoo() com: "+v);
		voo.put(v.getId(), v);

		// vê se a Base de dados dos destinos está vazia
		if(destinotable.isEmpty()){
			listadestinos=new ArrayList<Integer>();
			listadestinos.add(v.getId());
			destinotable.put(hashDestino(v.getLocalDestino()), listadestinos);
		}
		else
			if(destinotable.containsKey(hashDestino(v.getLocalDestino()))){
				ArrayList<Integer> lista=destinotable.get(hashDestino(v.getLocalDestino()));
				lista.add(v.getId());
				destinotable.put(hashDestino(v.getLocalDestino()), lista);
			}
			else{
				ArrayList<Integer> lista=new ArrayList<Integer>();
				lista.add(v.getId());
				destinotable.put(hashDestino(v.getLocalDestino()), lista);
			}

		if(datatable.isEmpty()){
			listadata=new ArrayList<Integer>();
			listadata.add(v.getId());
			datatable.put(v.getDataPartida(), listadata);
		}
		else
			if(datatable.containsKey(v.getDataPartida())){
				ArrayList<Integer> lista=datatable.get(v.getDataPartida());
				lista.add(v.getId());
				datatable.put(v.getDataPartida(), lista);
			}
			else{
				ArrayList<Integer> lista=new ArrayList<Integer>();
				lista.add(v.getId());
				datatable.put(v.getDataPartida(), lista);
			}
	}

	// Pesquisa o vôo por ID
	public Voo getVooPorId(int id) throws RemoteException {
		System.err.println("invocacao getVooPorId() com: "+id);
		if(voo.containsKey(id))
			return voo.get(id);
		else
			return null;
	}

	// Pesquisa o vôo numa determinada data
	public ArrayList<Voo> getVooPorData(String d) throws RemoteException {
		System.err.println("invocacao getVooPorData() com: "+d);
		int datavoo=Integer.parseInt(d);
		ArrayList<Voo> resultado=new ArrayList<Voo>();
		if(datatable.containsKey(datavoo)){
			ArrayList<Integer> datatemp=datatable.get(datavoo);
			for(int i=0;i<datatable.get(datavoo).size();i++){
				resultado.add(voo.get(datatemp.get(i)));
			}
		}
		return resultado;
	}

	// Pesquisa o vôo num determinado destino
	public ArrayList<Voo> getVooPorDestino(String d) throws RemoteException {
		System.err.println("invocacao getVooPorDestino() com: "+d);
		int destinovoo=hashDestino(d.toUpperCase());
		ArrayList<Voo> resultado=new ArrayList<Voo>();
		if(destinotable.containsKey(destinovoo)){
			ArrayList<Integer> datatemp=destinotable.get(destinovoo);
			for(int i=0;i<destinotable.get(destinovoo).size();i++){
				resultado.add(voo.get(datatemp.get(i)));
			}
		}
		return resultado;
	}

	// calcula a hash do destino para guardar na hashtable
	public int hashDestino(String s){
		int hash=0;
		int superhash=2;
		for(int i=0;i<s.length();i++){
			hash+=(superhash*(int) s.charAt(i));
			superhash++;
		}
		return hash;
	}

	// compra lugar(es) de um determinado vôo
	public String compraLugares(int id, int nr, String nomes){
		String check="";
		if(!voo.containsKey(id))
			check+="Vôo inexistente ou simulação de 9/11.";
			else
				synchronized(voo.get(id)){
					if((voo.get(id).getLugares()-nr)<0)
						check+="Não existe essa quantidade de lugares disponíveis." +
								"\nLugares disponíveis:"+voo.get(id).getLugares();
					else{
						String[] listanomes=nomes.split(";");
						if(nr!=listanomes.length){
							check+="Unidades pretendidas não coincide com nomes de passageiros introduzidos:\n" +
									listanomes.toString();
							return check;
						}
						voo.get(id).addPassageiros(nomes);
						check+="Lugar(es) reservado(s) para a data "+voo.get(id).getDataPartida()+":\n";
						for(int i=0;i<listanomes.length;i++)
							check+=listanomes[i]+"\n";
						check+="Efetue o pagamento no balcão.";
						}
				}
		return check;
	}

	// consulta a lista de passageiros de um determinado vôo
	public ArrayList<String> consultaPassageiros(int id){
		return voo.get(id).getListaPassageiros();
	}

	public String toString(){
		String s="";
		s+=voo.toString();
		s+="\n"+destinotable.toString();
		s+="\n"+datatable.toString();
		return s;
	}
}
