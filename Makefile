all:
	CLASSPATH=classes:. javac -d classes src/*.java

stubs:
	CLASSPATH=classes:. rmic -vcompat -d classes Database


# activar o servico de nomes:
reg:
	 rmiregistry -J-classpath -Jclasses:. 9000

# corre o servidor
runserver:
	CLASSPATH=classes:. java Server 9000

# corre um cliente
runclient:
	CLASSPATH=classes:. java Cliente



clean:
	rm -rf classes/* *~ src/*~
